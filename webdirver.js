const webdriver = require('selenium-webdriver')

const driver = new webdriver.Builder()
// "9515" 是ChromeDriver使用的端口
    .usingServer('http://localhost:9515')
    .withCapabilities({
        chromeOptions: {
            // 这里设置Electron的路径
            binary: 'D:\\myporjects\\my-electron-app\\out\\my-electron-app-win32-x64\\my-electron-app.exe'
        }
    })
    .forBrowser('electron')
    .build()

    driver.executeScript("document.addEventListener('click', function(e){alert('click2');});");