// preload.js

// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
window.addEventListener('DOMContentLoaded', () => {
    const replaceText = (selector, text) => {
      const element = document.getElementById(selector)
      if (element) element.innerText = text
    }
  
    for (const dependency of ['chrome', 'node', 'electron']) {
      replaceText(`${dependency}-version`, process.versions[dependency])
    }

    const toList = document.querySelector("#toList")
    const toListNewTab = document.querySelector("#toListNewTab")
    const toWebview = document.querySelector("#toWebview")
    const toIframe = document.querySelector("#toIframe")
    const ipc = require('electron').ipcRenderer
    if (toList) {
      toList.onclick = ()=>
        {
            ipc.send('toList');
        }
    }
    if (toListNewTab) {
      toListNewTab.onclick = ()=>
        {
            ipc.send('toListNewTab');
        }
    }
    if (toWebview) {
      toWebview.onclick = ()=>
        {
            ipc.send('toWebview');
        }
    }
    if (toIframe) {
      toIframe.onclick = ()=>
        {
            ipc.send('toIframe');
        }
    }


  const TabGroup = require("./index");

  let tabGroup = new TabGroup({
    newTab: {
      title: 'New Tab'
    }
  });

  tabGroup.addTab({
    title: 'Laiye',
    // src: 'https://laiye.com/',
    src: './list.html'
  });

  tabGroup.addTab({
    title: "Electron",
    // src: "http://electron.atom.io",
    src: './index.html',
    visible: true,
    active: true
  });

  console.log('execute preload.js...');
  })
  