// main.js

// Modules to control application life and create native browser window
const { app, BrowserWindow, session } = require('electron')
const path = require('path')
const url = require('url');
var mainWindow = null;
var mainWindowNew = null;
function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 800,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      // webSecurity: false
    }
  })


  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()
  mainWindow.webContents.closeDevTools();
  
}

function createWindowList (page) {
  // Create the browser window.
  let preloadFile = 'preload.js';
  if (page === 'electron-tabs.html') {
    preloadFile = 'preload-tabs.js';
  }
  mainWindowNew = new BrowserWindow({
    width: 1000,
    height: 1200,
    webPreferences: {
      webviewTag: true,
      preload: path.join(__dirname, preloadFile),
      // webSecurity: false
    }
  })


  // and load the index.html of the app.
  mainWindowNew.loadFile(page)

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()
  mainWindowNew.webContents.closeDevTools();
  
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// 部分 API 在 ready 事件触发后才能使用。
app.whenReady().then(async () => {
  createWindow()

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })

  //await session.defaultSession.loadExtension('D:\\laiye\\projects\\chrome-plugin-demo-master\\simple-chrome-plugin-demo')
  //await session.defaultSession.loadExtension('C:\\Users\\lei.cao\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\Extensions\\fmkadmapgofadopljbjfkapdkoienihi\\4.13.5_0')
  //await session.defaultSession.loadExtension('C:\\Users\\lei.cao\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\Extensions\\bmdblncegkenkacieihfhpjfppoconhi\\4.4.1_0')

})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

// app.commandLine.appendSwitch("disable-features", "OutOfBlinkCors");

// In this file you can include the rest of your app's specific main process
// code. 也可以拆分成几个文件，然后用 require 导入。

const ipc = require('electron').ipcMain;
//进行监控，如果有new-window 发送过来，则重新创建一个窗口，文件是list.html
ipc.on('toList',function() {
         mainWindow.loadURL(url.format({
         pathname: path.join(__dirname, 'list.html'),
         protocol: 'file:',
         slashes: true
     }))
})

ipc.on('toListNewTab',function() {

  createWindowList("list.html");
})

ipc.on('toWebview',function() {

  createWindowList("electron-tabs.html");
})

ipc.on('toIframe',function() {

  createWindowList("iframeWrapper.html");
})