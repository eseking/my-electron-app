// preload.js

// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
window.addEventListener('DOMContentLoaded', () => {
    const replaceText = (selector, text) => {
      const element = document.getElementById(selector)
      if (element) element.innerText = text
    }
  
    for (const dependency of ['chrome', 'node', 'electron']) {
      replaceText(`${dependency}-version`, process.versions[dependency])
    }

    const toList = document.querySelector("#toList")
    const toListNewTab = document.querySelector("#toListNewTab")
    const toWebview = document.querySelector("#toWebview")
    const toIframe = document.querySelector("#toIframe")
    const addCrossFrame = document.querySelector("#addCrossFrame")
    const ipc = require('electron').ipcRenderer
    if (toList) {
      toList.onclick = ()=>
        {
            ipc.send('toList');
        }
    }
    if (toListNewTab) {
      toListNewTab.onclick = ()=>
        {
            ipc.send('toListNewTab');
        }
    }
    if (toWebview) {
      toWebview.onclick = ()=>
        {
            ipc.send('toWebview');
        }
    }
    if (toIframe) {
      toIframe.onclick = ()=>
        {
            ipc.send('toIframe');
        }
    }
    if (addCrossFrame) {
      addCrossFrame.onclick = ()=>
        {
            const iframe = document.createElement('iframe');
            iframe.src = 'https://www.baidu.com';
            document.querySelector('.dynamicFrame').appendChild(iframe);
        }
    }

  console.log('execute preload.js...');
  })
  